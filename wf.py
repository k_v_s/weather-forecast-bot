from pyowm import OWM
from pyowm.utils.config import get_default_config
from config import owm_api_key


wf = '' #сюда будет сохраняться прогноз погоды

config_dict = get_default_config()
config_dict['language'] = 'ru'

owm = OWM(owm_api_key, config_dict)
mgr = owm.weather_manager()

def get_wf(place):
    if type(place) is str:
        observation = mgr.weather_at_place(place)

    elif type(place) is list:
        observation = mgr.weather_at_coords(place[0], place[1])

    else:
        pass

    w = observation.weather

    t = w.temperature("celsius")
    t1 = t['temp']
    t2 = t['feels_like']
    t3 = t['temp_max']
    t4 = t['temp_min']

    wi = w.wind()['speed']
    humi = w.humidity
    cl = w.clouds
    dt = w.detailed_status
    ti = w.reference_time('iso')
    pr = int(w.pressure['press'] * 100 // 133.322)
    vd = w.visibility_distance
    
    wf = "🌡***Температура: ***" + str(t1) + " °C" + "\n" + "👁***Ощущается как: ***" + str(t2) + " °C" + "\n" + "🌬***Скорость ветра:*** " + str(wi) + " м/с" + "\n" + "😉***Давление: ***" + str(pr) + " мм.рт.ст" + "\n" + "💧***Влажность: ***" + str(humi) + " %" + "\n" + "☂️***Погода: ***" + str(dt)

    return wf