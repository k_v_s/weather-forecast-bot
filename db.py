import sqlite3


def add_user(user):
    conn = sqlite3.connect('users.db')
    cur = conn.cursor()
    cur.execute("""INSERT OR REPLACE INTO users(user_id, fname, lname, username) VALUES(?, ?, ?, ?);""", user)
    conn.commit()
    conn.close()