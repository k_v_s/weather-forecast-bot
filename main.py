import telebot
from config import *
from wf import *
import cherrypy
from db import *

bot = telebot.TeleBot(token)


@bot.message_handler(commands=['start'])
def start(message):
    bot.reply_to(message, 'Привет! Для получения прогноза погоды отправьте свою геопозицию или название города: ')
    user = (message.chat.id, message.from_user.first_name, message.from_user.last_name, message.from_user.username)
    add_user(user)

@bot.message_handler(commands=['info'])
def info_msg(message):
    bot.reply_to(message, 'Данный бот использует API openweathermap.com для получения погоды в любой точке мира. Можно отправить название города(наприме, Москва) или точную геолокацию')


@bot.message_handler(content_types=['text'])
def send_wf_from_txt(message):
    try:
        bot.reply_to(message, get_wf(message.text), parse_mode='markdown')
    except Exception as e:
        print(e)
        bot.reply_to(message, 'Произошла неизвестная ошибка. Возможно, Вы неправильно написали название города или такого города не существует. Исправьте ошибку и попробуйте снова!', parse_mode='markdown')

@bot.message_handler(content_types=['location'])
def send_wf_from_loc(message):
    latitude = message.location.latitude
    longitude = message.location.longitude
    coords = [latitude, longitude]
    try:
        bot.reply_to(message, get_wf(coords), parse_mode='markdown')
    except Exception as e:
        print(e)
        bot.reply(message, 'Произошла неизвестная ошибка. Возможно, Вы неправильно написали название города или такого города не существует. Исправьте ошибку и попробуйте снова!', parse_mode='markdown')





class WebhookServer(object):
    @cherrypy.expose
    def index(self):
        if 'content-length' in cherrypy.request.headers and \
                        'content-type' in cherrypy.request.headers and \
                        cherrypy.request.headers['content-type'] == 'application/json':
            length = int(cherrypy.request.headers['content-length'])
            json_string = cherrypy.request.body.read(length).decode("utf-8")
            update = telebot.types.Update.de_json(json_string)
            # Эта функция обеспечивает проверку входящего сообщения
            bot.process_new_updates([update])
            return ''
        else:
            raise cherrypy.HTTPError(403)


bot.remove_webhook()

bot.set_webhook(url=WEBHOOK_URL_BASE + WEBHOOK_URL_PATH,
                certificate=open(WEBHOOK_SSL_CERT, 'r'))


# Указываем настройки сервера CherryPy
cherrypy.config.update({
    'server.socket_host': WEBHOOK_LISTEN,
    'server.socket_port': WEBHOOK_PORT,
    'server.ssl_module': 'builtin',
    'server.ssl_certificate': WEBHOOK_SSL_CERT,
    'server.ssl_private_key': WEBHOOK_SSL_PRIV
})

 # Собственно, запуск!
cherrypy.quickstart(WebhookServer(), WEBHOOK_URL_PATH, {'/': {}})